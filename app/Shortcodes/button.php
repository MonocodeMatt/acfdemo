<?php

namespace App\Shortcodes;

add_shortcode( 'button', __NAMESPACE__ . '\\button' );

function button( $args=[] ){

    $value = array_merge( [
       'link' => '/',
       'text' => 'Hello World',
       'color' => 'primary',
    ], $args );

    ob_start();

    ?>

    <a href="<?= $value['link']; ?>" class="btn btn-<?= $value['color']; ?>">
        <?= $value['text']; ?>
    </a>

    <?php

    $output = ob_get_contents();

    ob_end_clean();

    return $output;

}
