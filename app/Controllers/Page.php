<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use App\Controllers\Partials\hasFeaturedImage;
use App\Controllers\Partials\hasFooter;

class Page extends Controller {

    use hasFeaturedImage, hasFooter;

}
