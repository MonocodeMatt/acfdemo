<?php

namespace App\Controllers\Partials;

trait hasFeaturedImage {

    public function featuredimage() {

        return get_the_post_thumbnail_url();

    }

}
