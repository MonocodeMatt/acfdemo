<?php

Namespace Theme\Admin;

add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\loginStyles' );
add_filter( 'login_headerurl', __NAMESPACE__ . '\\loginLogoUrl' );

/**
 * Set Custom Logo for login screen
 */
function loginStyles() {

    /**
     * Include Styles
     */
    wp_enqueue_style('sage/login.css', \App\asset_path('styles/login.css'), array('login'));

    /**
     * Include Scripts
     */
    wp_register_script('sage/login.js', \App\asset_path('scripts/login.js'), ['jquery'], null, true);
    wp_enqueue_script( 'sage/login.js' );

}

/**
 * On Login, set logo url to point to website's homepage
 */
function loginLogoUrl() {

    return get_bloginfo('url');

}
