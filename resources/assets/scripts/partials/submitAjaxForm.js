import { updatePager } from './updatePager';

// Ajax Forms
// eslint-disable-next-line no-undef
const ajaxData = customAjax;
const listingClass = '.ajaxListing';
const loadingClass = 'loading';

// eslint-disable-next-line no-unused-vars
export const submitAjaxForm = function( data ){

  $( listingClass ).addClass( loadingClass );

  $.ajax( {
    type: 'post',
    dataType: 'json',
    url: ajaxData.ajaxurl,
    data: data,

    success: function( response ){
      $( listingClass ).html( response.response );
      updatePager( response.currentPage, response.totalPages );
    },

    error: function(){
      $( listingClass ).html( '<div class="alert alert-danger"><strong>An Error occurred:</strong> Please contact the website administrator about this.</div>')
    },

    complete: function(){
      $( listingClass ).removeClass( loadingClass );
    },

  } );

};

export default submitAjaxForm;
