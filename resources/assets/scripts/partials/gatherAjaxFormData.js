// eslint-disable-next-line no-unused-vars
export const gatherAjaxFormData = function ( form, formData = {} ){

  $.each( form.serializeArray(), function( i, field ){
    formData[ field.name ] = field.value;
  } );

  return formData

};

export default gatherAjaxFormData;
