export const normalizeSlideHeights = function (){

  $('.carousel').each(function(){

    var items = $('.carousel-item', this);

    // reset the height
    items.css('min-height', 0);

    // set the height
    var maxHeight = Math.max.apply(null,
      items.map(function(){
        return $(this).outerHeight()}).get() );

    items.find('.carouselBody').css('min-height', maxHeight + 'px');

  });

};

export default normalizeSlideHeights;
