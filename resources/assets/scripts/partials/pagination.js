import {gatherAjaxFormData} from './gatherAjaxFormData';
import {submitAjaxForm} from './submitAjaxForm';

export const pagination = function (){

  $('.pagination button').click( function( e ) {

    e.preventDefault();

    const thisForm = $(this).closest( 'form' );
    let formData = {};

    if( $(this)[0].name ){
      formData[ $(this)[0].name ] = $(this)[0].value;
    }

    formData = gatherAjaxFormData( thisForm, formData );

    submitAjaxForm( formData );

  })

};

export default pagination;
