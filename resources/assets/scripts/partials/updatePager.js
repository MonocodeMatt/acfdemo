export const updatePager = function( currentPage, totalPages ){
  const prevPager = $('.pagination .prev-page');
  const nextPager = $('.pagination .next-page');
  const currentContext = $('.pagination .currentPage');
  const totalContext = $('.pagination .totalPages');

  prevPager.prop( 'disabled', currentPage <= 1 );
  prevPager.value = currentPage - 1;

  nextPager.prop( 'disabled', currentPage >= totalPages );
  nextPager.value = currentPage + 1;

  currentContext.html( currentPage );
  totalContext.html( totalPages );

};

export default updatePager;
