// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// Import Partials
import { ieBodyClass } from './partials/ieBodyClass';
import { pagination } from './partials/pagination';
import { normalizeSlideHeights } from './partials/normalizeSlideHeights';

ieBodyClass();
pagination();


$(window).on(
  'load resize orientationchange',
  normalizeSlideHeights);
