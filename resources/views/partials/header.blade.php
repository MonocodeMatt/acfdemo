<header itemscope itemtype="https://schema.org/WPHeader">

  <div class="container">

    <nav itemscope itemtype="https://schema.org/SiteNavigationElement">

      <a class="logo" href="/">
        {!! file_get_contents( get_template_directory() . '/assets/images/full-logo.svg' ) !!}
      </a>

      <button class="navbar-toggler animatedClick" data-target="topNav" aria-controls="topNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="topNav" id="primaryNav">

        <div class="close animatedClick d-lg-none" data-target="topNav" aria-controls="topNav" aria-expanded="false" aria-label="Toggle navigation">
          <span aria-hidden="true">&times;</span>
        </div>

        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif

      </div>

    </nav>

  </div>

</header>
