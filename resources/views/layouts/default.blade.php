<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body data-spy="scroll" itemscope itemtype="http://schema.org/WebPage" data-target="#navbar-aside" data-offset="250" @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document" itemprop="mainEntity" @if( $schematype ) itemscope itemtype="{{ $schematype }}" @endif>
      <div class="content container">
        <div class="row flex-row-reverse">
          <main class="main col-sm">
            @yield('content')
          </main>
          @hasSection('sidebar')
            <aside class="sidebar d-none d-lg-block col-lg-3">
              @yield('sidebar')
            </aside>
          @endif
        </div>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
