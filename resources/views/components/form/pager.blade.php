<form method="POST">

  <input type="hidden" name="search" value="{{ $params['s'] }}" />
  <input type="hidden" name="action" value="search_updates" />
  <input type="hidden" name="nonce" value="{{ $nonce }}" />

  <form method="POST">
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-end">
        <li class="page-item">
          <button class="page-link prev-page" {{ $currentPage > 1? '' : 'disabled' }} name="paged" value="{{ $currentPage - 1 }}">Previous</button>
        </li>
        <li class="pagerContext">
          <span>Page <span class="currentPage">{{ $currentPage? $currentPage : 1 }}</span> of <span class="totalPages">{{ $totalPages }}</span></span>
        </li>
        <li class="page-item">
          <button class="page-link next-page" {{ ( $currentPage == $totalPages )? 'disabled' : '' }} name="paged" value="{{ $currentPage? $currentPage + 1 : 2 }}">Next</button>
        </li>
      </ul>
    </nav>
  </form>

</form>
