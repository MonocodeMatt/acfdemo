<form method="POST" id="searchForm">

  <input type="hidden" name="action" value="search" />
  <input type="hidden" name="nonce" value="{{ $nonce }}" />

  <div class="row mb-4">

    <div class="col-12 d-none">

      <label for="search">Search</label>

    </div>

    <div class="col">

      <input type="text" id="search" class="form-control" value="{{ $params['s'] }}" name="search" />

    </div>

    <div class="col-auto">

      <button type="submit" class="btn btn-primary">Search</button>

    </div>

  </div>

</form>
